angular
    .module('bzrdashApp')
    .controller('signinCtrl', ['$scope', '$location', '$window', 'authenticationService', '$timeout', '$rootScope',
        function signinCtrl($scope, $location, $window, authenticationService, $timeout, $rootScope) {
            $scope.userInfo = null;

            var working = false;




            $scope.login = function () {
                if (working) return;
                working = true;

                var $this = angular.element('.login'),
                    $state = $this.find('button > .state');
                $this.addClass('loading');
                $state.html('Authenticating');

                authenticationService.login($scope.userName, $scope.password)
                    .then(function (result) {
                        $this.addClass('ok');
                        $state.html('Welcome back!');
                        $scope.userInfo = result;
						$rootScope.userInfo = result;
                        $timeout(function () {
                            $rootScope.$apply();

                            $location.path("/");

                        }, 2000);

                    }, function (error) {
                        $window.alert("Invalid credentials");
                    });
            };

            $scope.cancel = function () {
                $scope.userName = "";
                $scope.password = "";
            };

        }]);
