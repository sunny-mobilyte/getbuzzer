angular
    .module('bzrdashApp')
    .service('QuestionsService', ['$http', 'authenticationService', 'SERVERURL',
        function ($http, authenticationService, SERVERURL) {

        var QuestionsService = {};

        QuestionsService.getQuestions = function () {
            return $http.get(SERVERURL.url + 'questions/' + authenticationService.getUserInfo().loginToken).then(function(res) {
                return res.data;
            });
        };

        return QuestionsService;
    }]);

