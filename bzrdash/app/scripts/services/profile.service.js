/**
Name : profileService
function : Used to communicate with the profile API
Parameters : $http, SERVERURL

*/
angular
.module('bzrdashApp')
.service('profileService', ['$q', '$http', 'SERVERURL','authenticationService', function ($q, $http, SERVERURL, authenticationService) {
     return {
          getProfile : function (cb) {
               var deferred = $q.defer();
               $http.get(SERVERURL.url + 'users/getProfile/' + authenticationService.getUserInfo().loginToken).then(function(result) {
                    deferred.resolve(result);
               }, function (error) {
                    deferred.reject(error);
               });
               cb(deferred.promise);
          },

          updateProfile : function (reqBody, cb) {
               var deferred = $q.defer();

               // var info = JSON.parse($window.sessionStorage["userInfo"]);
               // info.loginToken = '';

               $http({
                    url: SERVERURL.url + 'users/updateProfile',
                    method: 'POST',
                    data: reqBody,
                    headers: {
                         'Content-Type': 'application/json; charset=utf-8'
                    }
               }).then(function (result) {
                    deferred.resolve(result);
               }, function (error) {
                    deferred.reject(error);
               });

               cb(deferred.promise);
               // return $http.get(SERVERURL.url + '/users/updateProfile/' + authenticationService.getUserInfo().loginToken, reqBody).then(function(res) {
               //      return res.data;
               // });
          },

          updateQuestions : function (reqBody, cb) {
               var deferred = $q.defer();

               // var info = JSON.parse($window.sessionStorage["userInfo"]);
               // info.loginToken = '';

               $http({
                    url: SERVERURL.url + 'users/updateQuestions',
                    method: 'POST',
                    data: reqBody,
                    headers: {
                         'Content-Type': 'application/json; charset=utf-8'
                    }
               }).then(function (result) {
                    deferred.resolve(result);
               }, function (error) {
                    deferred.reject(error);
               });

               cb(deferred.promise);
               // return $http.get(SERVERURL.url + '/users/updateProfile/' + authenticationService.getUserInfo().loginToken, reqBody).then(function(res) {
               //      return res.data;
               // });
          }
     }

}]);
