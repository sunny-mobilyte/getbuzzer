angular
    .module('bzrdashApp')
    .service('FeedbacksService', ['$http', 'authenticationService', 'SERVERURL',
        function ($http, authenticationService, SERVERURL) {

        var FeedbacksService = {};

        FeedbacksService.getFeedbacks = function () {
            return $http({
                url: SERVERURL.url + 'feedbacks/' + authenticationService.getUserInfo().loginToken,
                method: 'GET'
            }).then(function(res) {
                return res.data;
            });
        };

        FeedbacksService.getNPS = function () {
            return $http.get(SERVERURL.url + 'getNPS/' + authenticationService.getUserInfo().loginToken).then(function(res) {
                return res.data;
            });
        };

        FeedbacksService.getServers = function () {
            return $http.get(SERVERURL.url + 'getServers/' + authenticationService.getUserInfo().loginToken).then(function(res) {
                return res.data;
            });
        };

        return FeedbacksService;
    }]);
