'use strict';

function wizardCtrl($scope) {
  $scope.validationOpt = {
    rules: {
      emailfield: {
        required: false,
        email: true,
        minlength: 3
      },
      namefield: {
        required: false,
        minlength: 3
      },
      passwordfield: {
        required: false,
        minlength: 6
      },
      cpasswordfield: {
        required: false,
        minlength: 8,
        equalTo: '#passwordfield'
      },
      description: {
        required: true
      },
      number: {
        required: true
      },
      name: {
        required: true
      },
      expiry: {
        required: true
      },
      cvc: {
        required: true
      }
    }
  };

  $scope.wizardOpt = {
    tabClass: '',
    'nextSelector': '.button-next',
    'previousSelector': '.button-previous',
    'firstSelector': '.button-first',
    'lastSelector': '.button-last',
    onNext: function () {
      var $valid = angular.element('#commentForm').valid(),
        $validator;
      if (!$valid) {
        $validator.focusInvalid();
        return false;
      }
    },
    onTabClick: function () {
      return false;
    }
  };
}

angular
  .module('bzrdashApp')
  .controller('wizardCtrl', ['$scope', wizardCtrl]);
