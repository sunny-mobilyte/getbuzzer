'use strict';

function dashboardCtrl($scope, $interval, COLORS, QuestionsService, FeedbacksService, WeeklySnapshotService, ReviewsService, authenticationService, auth) {
     //console.log(authenticationService.getUserInfo());
    $scope.userInfo = auth;

    var visits = [
        [1, 8],
        [2, 1],
        [3, 1],
        [4, 6],
        [5, 1]
    ];

    $scope.questions = [];

    $scope.lineDataset = [{
        data: visits,
        color: COLORS.success
    }];

    $scope.lineOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 1,
                fill: true
            },
            shadowSize: 0
        },
        grid: {
            color: COLORS.border,
            borderWidth: 0,
            hoverable: true,
        },
        xaxis: {
            min: 1,
            max: 5
        },
        yaxis: {
            min: 0,
            max: 10
        }
    };


    $scope.barDataset = [
        {
            data: [],
            bars: {
                show: true,
                barWidth: 0.8,
                align: 'center',
                fill: true,
                lineWidth: 0,
                fillColor: COLORS.default
            }
        }
    ];

    $scope.barFood = {
        show: true,
        barWidth: 0.8,
        align: 'center',
        fill: true,
        lineWidth: 0,
        fillColor: COLORS.default

    };


    $scope.barOptions = {
        grid: {
            hoverable: true,
            clickable: false,
            color: 'white',
            borderWidth: 0,
        },
        yaxis: {
            show: false
        },
        xaxis: {
            mode: 'categories',
            tickLength: 0,
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Roboto',
            axisLabelPadding: 5
        }
    };

    var seriesData = [[], [], []];
    var random = new Rickshaw.Fixtures.RandomData(150);

    for (var i = 0; i < 150; i++) {
        random.addData(seriesData);
    }


    $interval(function () {
        $scope.series = null;
        random.removeData(seriesData);
        random.addData(seriesData);

        $scope.series = [{
            data: seriesData[0],
        }, {
            data: seriesData[1],
        }];
    }, 1000);

    var seriesdataReviews = [[], [], []];
    var seriesdataNPS = [[], [], []];
    var randomNPS = new Rickshaw.Fixtures.RandomData(30);

    for (var v = 0; v < 30; v++) {
        randomNPS.addData(seriesdataReviews);
        randomNPS.addData(seriesdataNPS);
    }

// NPS graph

    $scope.optionsNPS = {
        renderer: 'area',
        height: 133,
        padding: {
            top: 2, left: 0, right: 0, bottom: 0
        },
        interpolation: 'cardinal',
        stroke: false,
        strokeWidth: 0,
        preserve: true,
    };

    $scope.featuresNPS = {

        hover: {
            xFormatter: function (x) {
                var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
                var date = new Date(x);
                //Wednesday 28 Sept 2016
                return days[date.getDay()] + ' ' + date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
            },
            yFormatter: function (y) {
                return Math.round(y);
            }
        }
    };

    $scope.seriesNPS = [{
        color: COLORS.primary,
        name: 'Average NPS&reg; for day',
        data: seriesdataNPS[0]
    }];

// Reviews graph

    $scope.optionsReviews = {
        renderer: 'area',
        height: 133,
        padding: {
            top: 0, left: 0, right: 0, bottom: 0
        },
        interpolation: 'cardinal',
        stroke: false,
        strokeWidth: 0,
        preserve: true,
    };

    $scope.featuresReviews = {

        hover: {
            xFormatter: function (x) {
                var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
                var date = new Date(x);
                //Wednesday 28 Sept 2016
                return days[date.getDay()] + ' ' + date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
            },
            yFormatter: function (y) {
                return Math.round(y);
            }
        }
    };

    $scope.seriesReviews = [{
        color: COLORS.primary,
        name: 'Total reviews',
        data: [
            {x: 1, y: 50},
            {x: 2, y: 72},
            {x: 3, y: 63},
            {x: 4, y: 44},
            {x: 5, y: 75},
            {x: 6, y: 66},
            {x: 7, y: 87}
        ]
    }];

    $scope.getArray = [{a: 1, b:2}, {a:3, b:4}];

    QuestionsService.getQuestions()
        .then(function (data) {
             console.log(data);
            $scope.questions = data;
        });

    $scope.totalReviews = 0;

    FeedbacksService.getFeedbacks()
        .then(function (data) {
            $scope.seriesReviews[0].data = data.chart;
            $scope.totalReviews = data.sum;
        });

    FeedbacksService.getNPS()
        .then(function (data) {
            $scope.seriesNPS[0].data = data.chart;
        });

    FeedbacksService.getServers()
        .then(function (data) {
            $scope.barDataset[0].data = data.chart;
            $scope.activeServer = data.activeServer;
        });

    WeeklySnapshotService.getWeeklySnapshots()
        .then(function (data) {
            $scope.weeklySnapshot = data;
        });

    ReviewsService.getReviews()
        .then(function (data) {
            $scope.reviews = data;
        });

    ReviewsService.getResponseRate()
        .then(function (data) {
            $scope.rate = data.rate;
        });

}

angular
    .module('bzrdashApp')
    .controller('dashboardCtrl', ['$scope', '$interval', 'COLORS', 'QuestionsService', 'FeedbacksService', 'WeeklySnapshotService', 'ReviewsService', 'authenticationService', 'auth', dashboardCtrl]);
