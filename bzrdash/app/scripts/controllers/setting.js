/**
Name : settingCtrl
function : Used to configre setting Detail
Parameters :

*/
angular
.module('bzrdashApp')
.controller('settingCtrl', ['$scope','$state','profileService','$rootScope','$window','authenticationService', 'QuestionsService',function settingCtrl($scope, $state, profileService, $rootScope, $window, authenticationService, QuestionsService) {

     var loginToken = Object.keys(authenticationService.getUserInfo()).length ? authenticationService.getUserInfo().loginToken : ''
     var questionsArr = [];

     /** Function to fetch user profile*/
     var getProfileInfo = function () {
          if(loginToken) {
               profileService.getProfile(function (promise) {
                    promise.then(function (res) {
                         $scope.Profile = res.data;
                    });
               });
          }
     }();

     /** Function to check index & add question if have no index*/
     var checkAndAdd = function (name) {
          //var id = arr.length + 1;
          var found = questionsArr.some(function (el) {
               return el.index === name;
          });
          if (!found) {
               questionsArr.push({index : name, active : false, question : '' });
          }
     }

     /** Function to fetch user questions*/
     if($state.current.name = 'app.forms.questions') {
          var getQuestions = function () {
               QuestionsService.getQuestions()
               .then(function (data) {
                    questionsArr = data;
                    var qArr = [1, 2, 3, 4, 5, 6, 7];
                    for(var key in qArr) {
                         checkAndAdd(qArr[key]);
                    }
                    questionsArr.sort(function(a, b){
                         return a.index-b.index
                    });
                    $scope.questionsArr = questionsArr;
                    //console.log(questionsArr);
               });
          }();
     }

     /** Function to update user profile*/
     $scope.updateUserDetails = function ( user) {
          profileService.updateProfile(user, function (promise) {
               promise.then(function (res) {
                    $scope.serverMsg = res.data;
               });
          })
     }

     /** Function to update questions*/
     $scope.updateQuestions = function ( questions) {
          profileService.updateQuestions($scope.questionsArr, function (promise) {
               promise.then(function (res) {
                    $scope.serverMsg = res.data;
               });
          })
     }



}]);
