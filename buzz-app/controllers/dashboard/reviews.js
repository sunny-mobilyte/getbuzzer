'use strict';

var _ = require('lodash');
var Feedbacks = require('../../models/feedback');

var getReviews = function getReviews(req, res, next) {

    var startDate = new Date();

    var oldDate = new Date(startDate.getTime() - (7 * 24 * 60 * 60 * 1000));
    var grandDate = new Date(startDate.getTime() - (14 * 24 * 60 * 60 * 1000));

    Feedbacks.aggregate([{
        $match: {
            outletID: req.user.shopName,
            _created_at: {$gte: oldDate}
        }
    }], function (err, feedbacks) {

        if (err) {
            return next(err);
        }

        Feedbacks.aggregate([{
            $match: {
                outletID: req.user.shopName,
                _created_at: {$gte: grandDate, $lt: oldDate}
            }
        }], function (err, prevFeedbacks) {

            if (err) {
                return next(err);
            }

            var upDown = 'down';

            var average = Math.round((feedbacks.length - prevFeedbacks.length) / prevFeedbacks.length * 100);

            if ( average > 0 ) {
                upDown = 'up'
            }

            res.send({
                number: average,
                upDown: upDown
            });
        });
    });
};

module.exports.getReviews = getReviews;
