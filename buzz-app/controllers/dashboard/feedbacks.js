'use strict';

var _ = require('lodash');
var Feedbacks = require('../../models/feedback');

var getFeedbacks = function getFeedbacks(req, res, next) {

    var now = new Date();

    var oldDate = new Date(now.getTime() - (7 * 24 * 60 * 60 * 1000));


    var sample = 30,
        Days = 7,
        OneDay = ( 1000 * 60 * 60 * 24 ),
        Today = now - ( now % OneDay ) ,
        nDaysAgo = Today - ( OneDay * Days ),
        startDate = new Date( nDaysAgo ),
        endDate = new Date( Today + OneDay ),
        store = {};

    var thisDay = new Date( nDaysAgo );
    while ( thisDay < endDate ) {
        store[thisDay] = 0;
        thisDay = new Date( thisDay.valueOf() + OneDay );
    }


    Feedbacks.aggregate([{
        $match: {
            outletID: req.user.shopName,
            _created_at: {$gte: oldDate}
        }
    }, {
        $project: {
            day: {$dateToString: {format: "%Y-%m-%d", date: "$_created_at"}},
            formID: '$formID',
        }
    }, {
        $group: {
            _id: '$day',
            feedbacks: {$addToSet: '$formID'}
        }

    }, {
        $sort: {_id: 1}
    }], function (err, feedbacks) {
        if (err) {
            return next(err);
        }

        feedbacks.forEach(function(result){
            store[new Date(result._id)] = result.feedbacks.length;
        });

        var updatedFeedbacks = [];
        var storedFeedbacks = [];
        var sum = 0;

        _.map(store, function (value, key) {
            var date = new Date(key);

            sum += value;
            storedFeedbacks.push({
                x: date.getTime(),
                y: value
            })
        });

        res.send({
            chart: storedFeedbacks,
            sum: sum
        });
    });
};

var getNPS = function getNPS(req, res, next) {

    var now = new Date();

    var oldDate = new Date(now.getTime() - (7 * 24 * 60 * 60 * 1000));


    var sample = 30,
        Days = 7,
        OneDay = ( 1000 * 60 * 60 * 24 ),
        Today = now - ( now % OneDay ) ,
        nDaysAgo = Today - ( OneDay * Days ),
        startDate = new Date( nDaysAgo ),
        endDate = new Date( Today + OneDay ),
        store = {};

    var thisDay = new Date( nDaysAgo );
    while ( thisDay < endDate ) {
        store[thisDay] = 0;
        thisDay = new Date( thisDay.valueOf() + OneDay );
    }

    Feedbacks.aggregate([{
        $match: {
            outletID: req.user.shopName,
            _created_at: {$gte: oldDate},
            question: 'How likely are you to recommend us to a friend?',
            type: 'n'
        }
    }, {
        $project: {
            day: {$dateToString: {format: "%Y-%m-%d", date: "$_created_at"}},
            tResponse: '$tResponse'
        }
    }, {
        $group: {
            _id: '$day',
            feedbacks: {$sum: 1},
            tResponse: {$push: '$tResponse'}
        }

    }, {
        $sort: {_id: 1}
    }], function (err, feedbacks) {
        if (err) {
            return next(err);
        }

        feedbacks.forEach(function(result){
            store[new Date(result._id)] = result.tResponse;
        });

        var updatedFeedbacks = [];

        _.map(store, function (value, key) {
            var date = new Date(key);

            updatedFeedbacks.push({
                x: date.getTime(),
                y: value
            })
        });


        _.forEach(updatedFeedbacks, function (feedback) {
            var redRatio = 0, ignoreRatio = 0, greenRatio = 0;
            var red = [];
            var ignore = [];
            var green = [];

            var totalLength = 0;

            _.forEach(feedback.y, function (value) {
                value = Number(value.toString().split('/')[0]);
                if (value < 6) {
                    red.push(value);
                }
                if (value > 6 && value < 8) {
                    ignore.push(value);
                }
                if (value > 8) {
                    green.push(value);
                }
            });

            totalLength = red.length + ignore.length + green.length;

            if (red.length) {
                redRatio = red.length / totalLength * 100;

            }

            if (green.length) {
                greenRatio = green.length / totalLength * 100;

            }

            feedback.y = greenRatio - redRatio;
            return feedback;
        });

        res.send({
            chart: updatedFeedbacks
        });
    });
};

var getServers = function getServers(req, res, next) {

    var startDate = new Date();

    var oldDate = new Date(startDate.getTime() - (7 * 24 * 60 * 60 * 1000));

    Feedbacks.aggregate([{
        $match: {
            outletID: req.user.shopName,
            _created_at: {$gte: oldDate},
            $text: {$search: 'served you today'},
            type: 'm',
            tResponse: {$nin: ['unknown', 'Unknown']}

        }
    }, {
        $project: {server: '$tResponse'}
    }, {
        $group: {
            _id: '$server',
            quantity: {$sum: 1}
        }

    }, {
        $sort: {_id: 1}
    }], function (err, servers) {
        if (err) {
            return next(err);
        }

        var updatedServers = [];
        _.chain(servers)
            .forEach(function (fb) {
                updatedServers.push(_.values(fb));
            })
            .value();

        var activeServer = _.maxBy(servers, function (server) {
            return server.quantity;
        });

        res.send({
            chart: updatedServers,
            activeServer: activeServer
        });
    });
};

module.exports.feedbacks = getFeedbacks;
module.exports.getNPS = getNPS;
module.exports.getServers = getServers;
