'use strict';

var _ = require('lodash');
var Feedbacks = require('../../models/feedback');

var getResponseRate = function getReviews(req, res, next) {

    var startDate = new Date();

    var oldDate = new Date(startDate.getTime() - (7 * 24 * 60 * 60 * 1000));

    Feedbacks.aggregate([{
        $match: {
            outletID: req.user.shopName,
            _created_at: {$gte: oldDate}
        }
    }], function (err, feedbacks) {

        if (err) {
            return next(err);
        }

        var expectedRate = 600;

        var average = Math.round( feedbacks.length / expectedRate * 100);
        res.send({
            rate: average
        });
    });
};

module.exports.getResponseRate = getResponseRate;
