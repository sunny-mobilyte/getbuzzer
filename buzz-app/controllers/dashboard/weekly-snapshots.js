'use strict';

var _ = require('lodash');
var Questions = require('../../models/questions');
var Feedbacks = require('../../models/feedback');

var getWeeklySnapshots = function getWeeklySnapshots(req, res, next) {

    var startDate = new Date();

    var oldDate = new Date(startDate.getTime() - (7 * 24 * 60 * 60 * 1000));
    var grandDate = new Date(startDate.getTime() - (14 * 24 * 60 * 60 * 1000));

    Questions.find({
        outletID: req.user.shopName,
        question: { $regex: /service|atmosphere|food/gmi }
    }).lean().exec(function (err, questions) {
        if (err) {
            return next(err);
        }
        Feedbacks.find({
            outletID: req.user.shopName,
            _created_at: {$gte: oldDate},
            question: {$regex: /service|atmosphere|food/gmi}
        }).lean().exec(function (err, feedbacks) {
            if (err) {
                return next(err);
            }

            Feedbacks.find({
                outletID: req.user.shopName,
                _created_at: {$gte: grandDate, $lt: oldDate},
                question: {$regex: /service|atmosphere|food/gmi}
            }).lean().exec(function (err, previosWeekFeedbacks) {
                if (err) {
                    return next(err);
                }

                var thisWeekResults = getResultsOfTheWeek(_.cloneDeep(questions), feedbacks);
                var previousWeekResults = getResultsOfTheWeek(_.cloneDeep(questions), previosWeekFeedbacks);

                var atmosphere;
                var food;
                var service;
                var value;

                if (thisWeekResults.m && previousWeekResults.m) {
                    atmosphere = compareResults(thisWeekResults.m.response.tResponse, previousWeekResults.m.response.tResponse, 'atmosphere');
                    value = compareResults(thisWeekResults.m.response.tResponse, previousWeekResults.m.response.tResponse, 'atmosphere');
                } else {
                    atmosphere = compareResults(1, 1, 'atmosphere');
                    value = compareResults(1, 1, 'value');
                }
                if (thisWeekResults.n && previousWeekResults.n) {
                    food = compareResults(thisWeekResults.n.response.tResponse, previousWeekResults.n.response.tResponse, 'food');
                } else {
                    food = compareResults(1, 1, 'food');
                }
                if (thisWeekResults.s && previousWeekResults.s) {
                    service = compareResults(thisWeekResults.s.response.nResponse, previousWeekResults.s.response.nResponse, 'service');
                } else {
                    service = compareResults(1, 1, 'service');
                }
                res.send({
                    atmosphere: atmosphere,
                    food: food,
                    service: service,
                    value: value
                });

            });
        })
    });
};

module.exports.getWeeklySnapshots = getWeeklySnapshots;

function compareResults(current, prev, word) {
    var success = 'bg-success';
    var primary = 'bg-primary';
    var warning = 'bg-warning';
    var better = 'better';
    var same = 'same';
    var worse = 'worse';
    var snapshot = {};

    var minBorder = prev - prev /10;
    var maxBorder = prev + prev /10;

    if ( minBorder > current ) {
        snapshot = {
            className: warning,
            text: 'the ' + word + ' was ' + worse + ' than last week'
        };
    }
    if ( minBorder <= current && current <= maxBorder ) {
        snapshot = {
            className: primary,
            text: 'the ' + word + ' was the ' + same + ' as last week'
        };
    }
    if ( current > maxBorder ) {
        snapshot = {
            className: success,
            text: 'the ' + word + ' was ' + better + ' than last week'
        };
    }

    return snapshot
}

function getResultsOfTheWeek(questions, feedbacks) {

    _.forEach(questions, function (question) {
        var fbs = _.filter(feedbacks, function (feedback) {
            return feedback.question === question.question;
        });

        question.response = {};

        question.response.nResponse = _.map(fbs, function (fb) {
            return fb.nResponse;
        });

        question.response.tResponse = _.map(fbs, function (fb) {
            return fb.tResponse;
        });

        switch (question.type) {
            case 's':
                var value;

                if (question.response.nResponse.length) {
                    value = question.response.nResponse.reduce(function (p, c) {
                            return p + c;
                        }) / question.response.nResponse.length;
                }

                if (!value) {
                    value = 0
                }

                question.response.nResponse = value;
                break;
            case 'm':

                question.response.tResponse = _.chain(question.response.tResponse)
                    .countBy()
                    .mapKeys(function (value, key) {
                        switch (value) {
                            case ('Great') :
                                key = 'Great';
                                break;
                            case ('Good') :
                                key = 'Good';
                                break;
                            case ('Average') :
                                key = 'Average';
                                break;
                            case ('Poor') :
                                key = 'Poor';
                                break;
                            case ('Rubbish') :
                                key = 'Rubbish';
                                break;
                        }
                        return key;
                    })

                    .value();

                var totalPoints = 0;
                var totalLength = 0;

                if (question.response.tResponse.Great) {
                    totalPoints += question.response.tResponse.Great * 2;
                    totalLength += question.response.tResponse.Great;
                }
                if (question.response.tResponse.Good) {
                    totalPoints += question.response.tResponse.Good;
                    totalLength += question.response.tResponse.Good;
                }
                if (question.response.tResponse.Poor) {
                    totalPoints -= question.response.tResponse.Poor;
                    totalLength += question.response.tResponse.Poor;
                }
                if (question.response.tResponse.Rubbish) {
                    totalPoints -= question.response.tResponse.Rubbish * 2;
                    totalLength += question.response.tResponse.Rubbish;
                }

                question.response.tResponse = totalPoints;
                break;
            case 'n':
                var value;

                if (question.response.tResponse.length) {
                    value = question.response.tResponse.reduce(function (p, c) {
                            p = Number(p.toString().split('/')[0]);
                            c = Number(c.toString().split('/')[0]);
                            return p + c;
                        }) / question.response.tResponse.length;
                }

                if (!value) {
                    value = 0
                }

                question.response.tResponse = value;

                break;
            case 't':
                break;
            default:
                break;
        }
    });

    return _.keyBy(questions, 'type');
}
