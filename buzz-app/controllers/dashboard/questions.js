'use strict';

var _ = require('lodash');
var Questions = require('../../models/questions');
var Feedbacks = require('../../models/feedback');

var getQuestions = function (req, res, next) {

    var startDate = new Date();

    var oldDate = new Date(startDate.getTime() - (7 * 24 * 60 * 60 * 1000));

    Questions.find({
        outletID: req.user.shopName,
		active: true,
        question: { $regex: /^(.(?!served|did you have to eat|to recommend us))*$/gmi }
    }).lean().exec(function (err, questions) {
        if (err) {
            return next(err);
        }
        Feedbacks.find({
            outletID: req.user.shopName,
            _created_at: {$gte: oldDate},
            question: { $regex: /^(.(?!served|did you have to eat|to recommend us))*$/gmi }
        }).lean().exec(function (err, feedbacks) {
            if (err) {
                return next(err);
            }

            _.forEach(questions, function (question) {
                var fbs = _.filter(feedbacks, function (feedback) {
                    return feedback.question === question.question;
                });

                question.response = {};

                question.response.nResponse = _.map(fbs, function (fb) {
                    return fb.nResponse;
                });

                question.response.tResponse = _.map(fbs, function (fb) {
                    return fb.tResponse;
                });

                switch (question.type) {
                    case 's':
                        var value;

                        if (question.response.nResponse.length) {
                            value = question.response.nResponse.reduce(function (p, c) {
                                if (typeof p === 'undefined') {
                                    p = 0;
                                }
                                if (typeof c === 'undefined') {
                                    c = 0;
                                }
                                    return p + c;
                                }) / question.response.nResponse.length;
                        }

                        if (!value) {
                            value = 0
                        }

                        var arr = new Array(parseInt(value));
                        arr.push(value % 1);
                        question.response.nResponse = arr;
                        break;
                    case 'm':

                        question.response.tResponse = _.chain(question.response.tResponse)
                            .countBy()
                            .mapKeys(function (value, key) {
                                switch (value) {
                                    case ('yes') :
                                        key = 'yes';
                                        break;
                                    case ('no') :
                                        key = 'no';
                                        break;
                                    case ('maybe') :
                                        key = 'maybe';
                                        break;


                                }
                                return key;
                            })
                            .map(function (value, key) {
                                return [key, value];
                            })
                            .value();
                        break;
                    case 'n':
                        var value;

                        if (question.response.tResponse.length) {
                            value = question.response.tResponse.reduce(function (p, c) {
                                    p = Number(p.toString().split('/')[0]);
                                    c = Number(c.toString().split('/')[0]);
                                    return p + c;
                                }) / question.response.tResponse.length;
                        }

                        if (!value) {
                            value = 0
                        }

                        var arr = new Array(parseInt(value));

                        question.response.tResponse = arr.push(value % 1);

                        break;
                    case 't':
                        break;
                    default:
                        break;
                }
            });
            _.remove(questions, function (question) {
                return question.type === 'server';
            });

            console.log(questions[0].response)
            res.send(questions);
        });

    });


};

module.exports = getQuestions;
