'use strict';

var _ = require('lodash');
var Outlets = require('../../models/outlets');

var getOutlet = function getOutlet(req, res, next) {


    Outlets.findOne({
            outletID: req.user.shopName
        }).lean().exec(function (err, outlet) {

        if (err) {
            return next(err);
        }

        res.send({
            outlet: outlet
        });
    });
};

module.exports.getOutlet = getOutlet;
