'use strict';

var Faq = require('../models/faq');

var getFaq = function(req, res, next) {
    Faq.find({}, function (err, faq) {
        if(err) {
            return next(err);
        }
        res.send(faq);
    });
};

module.exports = getFaq;