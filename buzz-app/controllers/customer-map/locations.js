'use strict';

var _ = require('lodash');
var mongoose = require('mongoose');
var Feedbacks = require('../../models/feedback');
var Users = require('../../models/users');

var getLocations = function getReviews(req, res, next) {

    var startDate = new Date();

    var oldDate = new Date(startDate.getTime() - (17 * 24 * 60 * 60 * 1000));
    var grandDate = new Date(startDate.getTime() - (24 * 24 * 60 * 60 * 1000));

    Feedbacks.find({
        outletID: req.user.shopName
        }

    ).distinct('_p_user').lean().exec(function (err, feedbacks) {
        if (err) {
            return next(err);
        }

        feedbacks = _.map(feedbacks, function (feedback) {
            return feedback.split('$')[1];
        });

        Users.aggregate([{
            $match: {
                _id: {$in : feedbacks}
            }
        }, {
            $project: { postcode: '$postcode'}
        }], function (err, users) {

            if (err) {
                return next(err);
            }
            users = _.map(users, function (user) {
                return user.postcode
            });

            res.send(users);

        });
    });
};

module.exports.getLocations = getLocations;
