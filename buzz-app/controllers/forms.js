'use strict';

var Forms = require('../models/forms');

var getForms = function(req, res, next) {
    Forms.find({}, function (err, forms) {
        if(err) {
            return next(err);
        }
        res.send(forms);
    });
};

module.exports = getForms;