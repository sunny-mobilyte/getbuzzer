var Account = require('../models/account');

module.exports = function(req, res, next) {
    Account.find({
        loginToken: req.params.loginToken
    }).lean().exec(function (err, user) {
        req.user = user[0];
        next();

    });
};
