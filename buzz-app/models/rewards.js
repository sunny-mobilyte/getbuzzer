'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RewardsSchema = new Schema({
    'outletID' : String,
    'reward' : String,
    'active' : Number,
    '_updated_at' : Date,
    '_created_at' : Date

});

RewardsSchema.set('collection', 'rewards');

var Rewards = mongoose.model('Rewards', RewardsSchema);

module.exports = Rewards;