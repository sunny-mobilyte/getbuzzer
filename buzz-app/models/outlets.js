'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OutletsSchema = new Schema({
    '_updated_at': Date,
    'points': Number,
    '_created_at': Date,
    'active': Boolean,
    'outletID': String,
    'name': String,
    'reward': String,
    'rewardInfo': String,
    'geoPoint': Array,
    'proximity': Boolean,
    'logo': String
});

OutletsSchema.set('collection', 'Outlets');

var Outlets = mongoose.model('Outlets', OutletsSchema);

module.exports = Outlets;