var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');

var crypto = require('crypto');
var jwt = require('jsonwebtoken');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
    return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
    return (this.provider !== 'local' || (password && password.length > 6));
};

var Account = new Schema({
    username: String,
    password: String ,
    hash: String,
    salt: String,
    email: String,
    placeOfUsingBuzzer: String,
    shopName: String,
    shopLocation: String,
    shopsLocationsQuantity: String,
    loginToken: {
        type: String
    },
    loginExpires: {
        type: Date
    }
});

/**
 * Hook a pre save method to hash the password
 */
Account.pre('save', function(next) {
    if (this.password && this.password.length > 6) {

        // if a salt is already generated, do not do it again
        if(!this.salt){
            this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
            this.password = this.hashPassword(this.password);
        }
    }

    next();
});

/**
 * Create instance method for hashing a password
 */
Account.methods.hashPassword = function(password) {
    if (this.salt && password) {
        return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
    } else {
        return password;
    }
};

/**
 * Create instance method for authenticating user
 */
Account.methods.authenticate = function(password) {
    return this.password === this.hashPassword(password);
};

/**
 * Find possible not used username
 */
Account.statics.findUniqueUsername = function(username, suffix, callback) {
    var _this = this;
    var possibleUsername = username + (suffix || '');

    _this.findOne({
        username: possibleUsername
    }, function(err, user) {
        if (!err) {
            if (!user) {
                callback(possibleUsername);
            } else {
                return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
            }
        } else {
            callback(null);
        }
    });
};


Account.set('collection', 'Accounts');


module.exports = mongoose.model('Accounts', Account);
