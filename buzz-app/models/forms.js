'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FormsSchema = new Schema({
    'rewardInfo': String,
    'outletID': String,
    'server': String,
    'outletName': String,
    'formID': String,
    'reward': String,
    'feedback': Array,
    'logo': String,
    '_p_user': String,
    'claimed': Boolean,
    'points': Number,
    '_created_at': Date,
    '_updated_at': Date,
    'claimedDate': Date
});

FormsSchema.set('collection', 'Forms');

var Forms = mongoose.model('Forms', FormsSchema);

module.exports = Forms;