'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FaqSchema = new Schema({
    'question': String,
    'answer': String,
    'index': Number,
    '_updated_at': Date,
    '_created_at': Date
});

FaqSchema.set('collection', 'faq');

var Faq = mongoose.model('Faq', FaqSchema);

module.exports = Faq;