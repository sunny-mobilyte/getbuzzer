'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionsSchema = new Schema({
    'active': Boolean,
    '_created_at': Date,
    '_updated_at': Date,
    'index': Number,
    // 'options': Array,
    'outletID': String,
    'question': String,
    'type': String
});

QuestionsSchema.set('collection', 'Questions');

var Questions = mongoose.model('Questions', QuestionsSchema);

module.exports = Questions;
