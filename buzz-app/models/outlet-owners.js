'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OutletOwnersSchema = new Schema({
    'planID' : String,
    'planType' : String,
    'planPaid' : Number,
    '_p_user' : String,
    'outletID' : String,
    'paymentID' : String,
    '_updated_at' : Date,
    '_created_at' : Date
});

OutletOwnersSchema.set('collection', 'outletOwners');

var OutletOwners = mongoose.model('OutletOwners', OutletOwnersSchema);

module.exports = OutletOwners;