'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlansSchema = new Schema({
    'title' : String,
    'benefits' : String,
    'type' : String,
    '_updated_at' : Date,
    '_created_at' : Date
});

PlansSchema.set('collection', 'plans');

var Plans = mongoose.model('Plans', PlansSchema);

module.exports = Plans;