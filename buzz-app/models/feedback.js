'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FeedbackSchema = new Schema({
    'tResponse': String,
    'question': String,
    'type': String,
    '_p_user': String,
    'nResponse': Number,
    'outletID': String,
    'formID': String,
    '_created_at': Date,
    '_updated_at': Date
});

FeedbackSchema.set('collection', 'Feedback');

var Feedback = mongoose.model('Feedback', FeedbackSchema);

module.exports = Feedback;