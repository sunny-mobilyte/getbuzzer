'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    'gender': String/*,
    'email': String,
    'picture': String,
    'role': String*/
});

userSchema.set('collection', '_User');

var Users = mongoose.model('_User', userSchema, '_User');

module.exports = Users;