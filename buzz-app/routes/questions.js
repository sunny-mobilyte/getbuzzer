var express = require('express');
var router = express.Router();
var getQuestions = require('../controllers/dashboard/questions');
var getUser = require('../middleware/getUser')

/* GET users listing. */
router.get('/:loginToken', getUser, getQuestions);

module.exports = router;
