var express = require('express');
var router = express.Router();
var getResponseRate = require('../controllers/dashboard/response-rate').getResponseRate;
var getUser = require('../middleware/getUser')

/* GET users listing. */
router.get('/:loginToken', getUser, getResponseRate);

module.exports = router;
