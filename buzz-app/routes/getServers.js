var express = require('express');
var router = express.Router();
var getServers = require('../controllers/dashboard/feedbacks').getServers;
var getUser = require('../middleware/getUser');

/* GET users listing. */
router.get('/:loginToken', getUser, getServers);

module.exports = router;
