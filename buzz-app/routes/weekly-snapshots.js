var express = require('express');
var router = express.Router();
var getWeeklySnapshots = require('../controllers/dashboard/weekly-snapshots').getWeeklySnapshots;
var getUser = require('../middleware/getUser')

/* GET users listing. */
router.get('/:loginToken', getUser, getWeeklySnapshots);

module.exports = router;
