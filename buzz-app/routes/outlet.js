var express = require('express');
var router = express.Router();
var getOutlet = require('../controllers/common/outlet').getOutlet;
var getUser = require('../middleware/getUser');

/* GET users listing. */
router.get('/:loginToken', getUser, getOutlet);

module.exports = router;
