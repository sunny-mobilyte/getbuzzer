var express = require('express');
var router = express.Router();
var getNPS = require('../controllers/dashboard/feedbacks').getNPS;
var getUser = require('../middleware/getUser')

/* GET users listing. */
router.get('/:loginToken', getUser, getNPS);

module.exports = router;
