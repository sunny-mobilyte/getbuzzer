var express = require('express');
var router = express.Router();
var getLocations = require('../controllers/customer-map/locations').getLocations;
var getUser = require('../middleware/getUser')

/* GET users listing. */
router.get('/:loginToken', getUser, getLocations);

module.exports = router;
