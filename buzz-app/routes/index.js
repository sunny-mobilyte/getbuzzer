var passport = require('passport');
var Account = require('../models/account');
var jwt = require('jwt-simple');

var express = require('express');
var router = express.Router();

var users = require('./users');
var questions = require('./questions');
var feedbacks = require('./feedbacks');
var getNPS = require('./getNPS');
var getServers = require('./getServers');
var getWeeklySnapshots = require('./weekly-snapshots');
var getReviews = require('./reviews');
var getResponseRate = require('./response-rate');
var getLocations = require('./locations');
var getOutlet = require('./outlet');


/* GET home page. */
router.get('/', function(req, res) {
  res.send('Hello World!');
});

router.use('/users', users);
router.use('/questions', questions);
router.use('/feedbacks', feedbacks);
router.use('/getNPS', getNPS);
router.use('/getServers', getServers);
router.use('/getWeeklySnapshots', getWeeklySnapshots);
router.use('/getReviews', getReviews);
router.use('/getResponseRate', getResponseRate);
router.use('/getLocations', getLocations);
router.use('/getOutlet', getOutlet);

router.get('/register', function(req, res) {
  res.render('register', {});
});

router.post('/register', function(req, res, next) {
  console.log('registering user', req.body);

    // token expire time
    var expireTime = Date.now() + (2 * 60 * 60 * 1000); // 2 hours from now

    // generate login token
    var tokenPayload = {
        username: req.body.username,
        loginExpires: expireTime
    };

	var secret = 'keepitquiet';
    var loginToken = jwt.encode(tokenPayload, secret);

  var acc = new Account({
      username: req.body.username,
      email: req.body.email,
      placeOfUsingBuzzer: req.body.placeOfUsingBuzzer,
      shopName: req.body.shopName,
      shopLocation: req.body.shopLocation,
      shopsLocationsQuantity: req.body.shopsLocationsQuantity,
      password: req.body.password,
      provider: 'local',
      loginToken: loginToken,
      loginExpires: expireTime
  });
  acc.save(function(err, user, info) {
    if (err) {
      console.log('error while user register!', err);
      return next(err);
    }

      var token;

      // If a user is found
      if(user){
          user.password = undefined;
          user.salt = undefined;

          // token = user.generateJwt();
          res.status(200);
          res.json(user);
      } else {
          // If user is not found
          res.status(401).json(info);
      }
  });

});

router.get('/login', function(req, res) {
  res.send({user: req.user});
});

/**
 * todo: remove adding primary this code later
 * and move other code in to another file
 */

/*Account.register(new Account({username: 'barnabees', password: '1234'}), '1234', function(err) {
    if (err) {
        console.log('error while user register!', err);
        // return next(err);
    }

    console.log('user registered!');

});*/

router.post('/login', /*passport.authenticate('local', {}),*/ function(req, res, next) {

    passport.authenticate('local-token', {session: true}, function(err, user, info) {
        if (err || !user) {
            res.status(400).send(info);
        } else {

            // Remove sensitive data before returning
            user.password = undefined;
            user.salt = undefined;

            // return the user object (contains loginToken)

            console.log(user)


            req.login(user, function(err) {
                if (err) { return res.send(err); }
                return res.json(user);// <--
            });

        }
    })(req, res, next);

});


router.get('/logout', function(req, res) {
    req.logout();
    res.send(200);
});


module.exports = router;
