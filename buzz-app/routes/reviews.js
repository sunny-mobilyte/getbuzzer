var express = require('express');
var router = express.Router();
var getReviews = require('../controllers/dashboard/reviews').getReviews;
var getUser = require('../middleware/getUser')

/* GET users listing. */
router.get('/:loginToken', getUser, getReviews);

module.exports = router;
