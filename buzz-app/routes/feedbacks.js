var express = require('express');
var router = express.Router();
var getFeedbacks = require('../controllers/dashboard/feedbacks').feedbacks;
var getNPS = require('../controllers/dashboard/feedbacks').getNPS;
var getUser = require('../middleware/getUser')


/* GET users listing. */
router.get('/:loginToken', getUser, getFeedbacks);

module.exports = router;
