var express = require('express');
var router = express.Router();
var getUser = require('../middleware/getUser');
var Account = require('../models/account');

/* GET users listing. */
router.get('/:loginToken', getUser, function(req, res) {
     res.send('respond with a resource');
});

router.get('/getProfile/:loginToken', getUser, function(req, res) {
     Account.findOne({
          _id : req.user._id
     },
     {
          shopLocation : 1,
          shopName : 1,
          username : 1,
          email : 1

     }, function (err, response) {
          if (err) {
              return next(err);
         }
         res.send(response);
     })
});

router.post('/updateProfile', function(req, res) {
     if(req.body.password && req.body.confirm_password && req.body.password  != req.body.confirm_password) {
           res.send({resStatus : 'error', msg : 'Passwords do not match'});
     } else {
          req.body.password = "";
          req.body.confirm_password = "";
     }
     console.log(req.body);
});

module.exports = router;
