var fs = require('fs');
var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');


var mongoStore = require('connect-mongo')(session);

// var MS = require('express-mongoose-store')(session, mongoose);
var cors = require('cors');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var routes = require('./routes/index');

// mongoose.Promise = global.Promise;

var ca = fs.readFileSync(__dirname + "/ssl/SSLCA.pem");

// var db = mongoose.connect('mongodb://localhost/buzz');
var db = mongoose.connect('mongodb://buzzer-1319:SpvM47v!eS*n78NL@db-buzzer-1319.nodechef.com:5367/buzzer?ssl=true',
    {
        server: {
            sslCA: ca
        }
    });

var app = express();

// Passing the request url to environment locals
app.use(function(req, res, next) {
    res.locals.url = req.protocol + '://' + req.headers.host + req.url;
    next();
});

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride());

app.use(cors());
app.use(logger('dev'));


// CookieParser should be above session
app.use(cookieParser('buzz_secret'));

app.use(session({
    saveUninitialized: true,
    rolling: true,
    resave: true,
    key: 'asdasdasd',
    secret: 'buzz_secret',
    store: new mongoStore({
        db: db.connection.db,
        collection: 'sessions'
    }),
    cookie  : {
        secure: false,
        // domain: 'localhost',
        httpOnly: false,
        maxAge: 2 * 60 * 60 * 1000
    }
}));

app.use(passport.initialize());
app.use(passport.session());



var User = require('./models/account');


// Serialize sessions
passport.serializeUser(function(user, done) {
    done(null, user._id);
});

// Deserialize sessions
passport.deserializeUser(function(id, done) {

    User.findOne({
        _id: id
    }, '-salt -password', function(err, user) {
        done(err, user);
    });
});

var jwt = require('jwt-simple');
var secret = 'keepitquiet';

app.use('*', function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'origin, x-requested-with, content-type, accept, x-xsrf-token, X-HTTP-Method-Override');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
    function(username, password, done) {
        User.findOne({
            username: username
        }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, {
                    message: 'Unknown user or invalid password'
                });
            }
            if (!user.authenticate(password)) {
                return done(null, false, {
                    message: 'Unknown user or invalid password'
                });
            }

            return done(null, user);
        });
    }
));

passport.use('local-token', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
    function(username, password, done) {

        User.findOne({
            username: username
        }, function(err, user) {
            if (err) {
                console.log(err);
                return done(err);
            }
            if (!user) {
                return done(null, false, {
                    message: 'Unknown user or invalid password'
                });
            }
            if (!user.authenticate(password)) {
                return done(null, false, {
                    message: 'Unknown user or invalid password'
                });
            }

            // token expire time
            var expireTime = Date.now() + (2 * 60 * 60 * 1000); // 2 hours from now

            // generate login token
            var tokenPayload = {
                username: user.username,
                loginExpires: expireTime
            };

            var loginToken = jwt.encode(tokenPayload, secret);

            // add token and exp date to user object
            user.loginToken = loginToken;
            user.loginExpires = expireTime;

            // save user object to update database
            user.save(function(err) {
                if(err){
                    done(err);
                } else {
                    done(null, user);
                }
            });
        });
    }
));


app.use(require('./routes'));

var getUser = require('./middleware/getUser');

app.get('/qwerty', getUser, function(req, res) {
    console.log('login POST')
    res.redirect('/');
});

app.use(express.static(__dirname + '/'));

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        console.log('Err', err)
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    // res.status(err.status || 500);
    res.send({
        message: err.message,
        error: {}
    });
});


module.exports = app;
